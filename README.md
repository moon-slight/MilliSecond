# MilliSecond
>使用Unity開發，以C#撰寫

>結合 wii remote 和 Xbox kinect 的一款射擊遊戲

### Picture

Interface

![Interface](https://c1.staticflickr.com/5/4240/35164669430_79d45c79b1_b.jpg)

Game Mode

![GameMode](https://c1.staticflickr.com/5/4215/35164660620_7b8a874f6a_c.jpg)

Result

![Result](https://c1.staticflickr.com/5/4290/35421231231_bfbaf23bb1_c.jpg)

## Feature

#### Slow Motion
>When bullet near your head, system will automatically turn on slow motion, let player can dodge the bullet

![SlowMotion](https://c1.staticflickr.com/5/4241/35183939610_fe7e309408_z.jpg)


## Demo
https://www.youtube.com/watch?v=e36zeLi6DQE
